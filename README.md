# NAME

Pod::Classdoc - Generate Javadoc-like detail class documentation from POD

# SYNOPSIS

        my $classdocs = Pod::Classdoc->new($path, $title, $verbose);
        #
        #       generate classdocs for an entire project directory
        #
        $classdocs->openProject(@srclibs) or die $@;
        #
        #       add source code of a single module
        #
        $classdocs->add($some_source, $filename) or die $@;
        #
        #       add source code of a single module from its file
        #
        $classdocs->open($srcfile) or die $@;
        #
        #       render the classdocs for all added/opened modules,
        #       and write them out
        #
        $classdocs->writeClassdocs or die $@;
        #
        #       get a table of contents for the classdocs, with modules
        #       in the order specified
        #
        my $toc = $classdocs->getTOC( Order => \@order);
        #
        #       write a table of contents for the classdocs
        #       
        $classdocs->writeTOC( Order => \@order ) or die $@;
        #
        #       get a frame container document
        #
        my $frames = $classdocs->getFrameContainer();
        #
        #       write a frame container
        #       
        $classdocs->writeFrameContainer( 'index.html' ) or die $@;
        #
        #       clear out the loaded modules to start anew
        #       
        $classdocs->clear();
        #
        #       change the output path
        #       
        $oldpath = $classdocs->path( $newpath );

# DESCRIPTION

Using either an input filename, or source code as a scalar string,
or a list of source directories,
collects and scans the source code for classdoc formatted POD sections
to retrieve classnames, base classes, exported and imported symbols,
and public methods and members, as well as any classdoc markup tags. When
all files have been processed, a subclassing/inheritance cross reference
is generated, and the accumulated documentation is formatted
into a HTML document and written to a classdoc directory, along with
an optional index file.

Where applicable, classdoc tags are the same as javadoc; see
[http://java.sun.com/j2se/javadoc/writingdoccomments/](http://java.sun.com/j2se/javadoc/writingdoccomments/)
for a complete discussion.

Note that the @version and @serial/@serialField/@serialData
tags are not supported; @version is replaced by the package $VERSION
variable, and @serial is not relevant.

Also note that @param values are interpretted as hash-based
arguments if the param name is not preceded by any of \[$%@\*&\].

## Supported Tags

#### @author _Name_

(package level only) Specifies package author

#### @constructor

(method level only) Specifies the method is a constructor

#### @deprecated

Indicates the package or sub is deprecated.

#### @exports _NAME_  _description_

(package level only) List symbols exported by the package

#### @ignore

Omit the package/method from classdocs.

#### @imports _NAME_  _description_

(package level only) List symbols that can be imported by the package

#### @instance _array | hash | scalar | handle_ 

(package level only) Specifies the base type of an instance; useful
when public members are specified (see [@member](https://metacpan.org/pod/@member))

#### @member _NAME_  _description_

(package level only) List public members of class instances

#### @optional `variable`  _description_

(method level only) Specifies an optional parameter

#### @param `variable`  _description_

(method level only) Specifies a required parameter

#### @return _description_

(method level only) Specifies returned value in scalar context

#### @returnlist _description_

(method level only) Specify returned values in list context
(if different from scalar context)

#### @see _URL_

Specifies reference documentation links. Note that links to CPAN packages can be abbreviated
as `<cpan>Package::Name</cpan>`. Links to methods, members, and export or import
symbols (either local to the class, or as part of another classdoc package)
can be specified using the tags described in ["Special URL Tags"](#special-url-tags).

#### @self `$objectvar`

Specify the conventional variable name used for the object's instance
(e.g., '$this', '$self', '$obj', etc.)

#### @simplex

(method level only) to specify a simplex method (for [Thread::Apartment](https://metacpan.org/pod/Thread::Apartment) or
[Thread::Sociable::Apartment](https://metacpan.org/pod/Thread::Sociable::Apartment) based packages)

#### @since _date_

Specifies original version date.

#### @static

(method level only) Specifies the sub is a static class method (rather
than an instance method).

#### @urgent

(method level only) Specifies an urgent method (for [Thread::Apartment](https://metacpan.org/pod/Thread::Apartment) or
[Thread::Sociable::Apartment](https://metacpan.org/pod/Thread::Sociable::Apartment) based packages)

#### @xs `method_name`

(method level only) Specifies classdocs for XS methods (or any methods without a Perl implementation)

## Source Format

Classdoc sections are introduced by `=begin classdoc` and closed by
`=end classdoc` POD directives. Intervening lines are simple free format text,
except that classdoc tags must be specified as the first element on a line.

E.g.,

        =pod

        =begin classdoc

        @xs InCaseItsAnXSsub

        Description of the class/interface/method
        continues up to the first tag, or the end of the
        classdoc section, and may include HTML tags

        @author D. Arnold
        @since  2005-12-09
        @see    <a href="http://java.sun.com/j2se/javadoc/writingdoccomments/">Javadoc Reference</a>
        @see    <cpan>Some::Module::On::CPAN</cpan>
        @see    <method>another_method</method>
        @see    <method>DBI::errstr</method>
        @instance hash
        @self   $obj

        @param  $myVal          a description of the parameter being passed
                which may wrap to the next
                few
                lines
        @optional $myOptional   a description of an optional parameter

        @return  the scalar return value description which
                may flow onto the next lines

        @returnlist     the list context return value description

        =end classdoc

        =cut

A classdoc section immediately preceding a `package` keyword
is assumed to be package level; all others are assumed to apply
to the immediately succeding `sub`, except for classdocs
which include an `@xs` qualifier.

The output is formatted into

- an introductory package/class description
- a listing of exported and imported symbols, and any public instance members,
with associated descriptions
- a table of constructor and method summaries, which includes only an example
of the method's usage, and the first sentence
of the method's description text
- a listing of constructor and method details, which provides the complete
description and associated markup tags

Note that hash-based parameter list
elements are presented as "Name => value,".

### Special URL Tags

As a convenience, several special URL tags are supported, including

- **<cpan>**

    "<cpan>modulename</cpan>" will be expanded into
    the equivalent search.cpan.org URL hyperlink for the specified module name.

- **<export>**

    "<export>name</export>" will be expanded into
    the equivalent internal hyperlink for the specified exported symbol.

    "<export>Some::Package::name</export>" will attempt expansion of the
    package name into a filepath, with the internal exports symbol link
    appended.

- **<import>**

    "<import>name</import>" will be expanded into
    the equivalent internal hyperlink for the specified imported symbol.

    "<import>Some::Package::name</import>" will attempt expansion of the
    package name into a filepath, with the internal imports symbol link
    appended.

- **<member>**

    "<member>name</member>" will be expanded into
    the equivalent internal hyperlink for the specified public instance member.

    "<member>Some::Package::name</member>" will attempt expansion of the
    package name into a filepath, with the internal member link
    appended.

- **<method>**

    "<method>name</method>" will be expanded into
    the equivalent internal hyperlink for the specified public method.

    "<method>Some::Package::name</method>" will attempt expansion of the
    package name into a filepath, with the internal method link
    appended.

    Constructors are tagged as methods.

- **<package>**

    "<package>Some::Package</package>" will attempt expansion of the
    package name into a filepath.

# METHODS

Refer to the included classdocs for method descriptions.

### Notes

Note that `@param` and `@optional` tags should be specified
in the order they are expected by the method (except for named
parameter lists, which may be specified in any order).

Also note that embedded HTML tags are preserved in the output, but embedded
POD tags are **NOT** processed.

# TO DO

- Support method prototype parsing.
- Support method attribute specification.

# SEE ALSO

[Pod::ClassDocs::Project](https://metacpan.org/pod/Pod::ClassDocs::Project) - used to generate complete project
documents from [Pod::ProjectDocs](https://metacpan.org/pod/Pod::ProjectDocs) output, merged with classdocs
rendered from this module, and a table of contents rendered
from [HTML::ListToTree](https://metacpan.org/pod/HTML::ListToTree).

[http://www.presicient.com/classdoc/classdocs](http://www.presicient.com/classdoc/classdocs) for an example
of classdocs generated from this package.

# AUTHOR, COPYRIGHT, and LICENSE

Copyright(C) 2007, Dean Arnold, Presicient Corp., USA. All rights reserved.

Permission is granted to use this software under the same terms as Perl itself.
Refer to the [Perl Artistic License](https://metacpan.org/pod/perlartistic) for details.
